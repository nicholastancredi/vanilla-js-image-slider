/*
Author: Able Sense Media
Web: ablesense.com
Date of creation: 2014/01/01
*/

var APP = (function () {
	var me = {},
		browser = {}

	/////////////////////////////////////////////////////////////////
	////////////////////// PRIVATE FUNCTIONS ////////////////////////
	/////////////////////////////////////////////////////////////////
		//private vars
		;

	function getSVGsupport() {
		return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
	}

	function getMQsupport() {
		return (typeof window.matchMedia == 'function');
	}

	function isTouch() {
		return 'ontouchstart' in window || 'onmsgesturechange' in window;
	}

	function getAnimationSupport() {
		var b = document.body || document.documentElement,
			s = b.style,
			p = 'animation';

		if (typeof s[p] == 'string') { return true; }

		// Tests for vendor specific prop
		var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
		p = p.charAt(0).toUpperCase() + p.substr(1);

		for (var i=0; i<v.length; i++) {
			if (typeof s[v[i] + p] == 'string') { return true; }
		}

		return false;
	}

	function getFlexboxSupport() {
		var props = ['-webkit-flex', '-ms-flexbox', 'flex'],
			len = props.length,
			detect = document.createElement('div'),
			supported = false;

		while(len-- && !supported) {
			var val = props[len];
			detect.style.display = val;
			supported = (detect.style.display === val);
		}

		return supported;
	}

	function getNthChildSupport() {
		// selectorSupported lovingly lifted from the mad italian genius, Diego Perini
		// http://javascript.nwbox.com/CSSSupport/

		var support,
			sheet,
			doc = document,
			root = doc.documentElement,
			head = root.getElementsByTagName('head')[0],
			impl = doc.implementation || {
				hasFeature: function() {
					return false;
				}
			},
			selector = ':nth-child(2n+1)',
			link = doc.createElement("style");

		link.type = 'text/css';

		(head || root).insertBefore(link, (head || root).firstChild);

		sheet = link.sheet || link.styleSheet;

		if (!(sheet && selector)) return false;

		support = impl.hasFeature('CSS2', '') ?

		function(selector) {
			try {
				sheet.insertRule(selector + '{ }', 0);
				sheet.deleteRule(sheet.cssRules.length - 1);
			} catch (e) {
				return false;
			}
			return true;
		} : function(selector) {
			sheet.cssText = selector + ' { }';
			return sheet.cssText.length !== 0 && !(/unknown/i).test(sheet.cssText) && sheet.cssText.indexOf(selector) === 0;
		};

		return support(selector);
	}

	function getViewportSize() {
		browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	}

	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			if (immediate && !timeout) func.apply(context, args);
		};
	}

	/////////////////////////////////////////////////////////////////
	////////////////////// PUBLIC FUNCTIONS /////////////////////////
	/////////////////////////////////////////////////////////////////

	me.cancelEvent = function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
	};

	me.onResize = function(callback) {
		callback();

		$(window).on('resize', debounce(function() {
			callback();
		}, 200));
	};


	browser.supportsAnimation = getAnimationSupport();
	browser.supportsSVG = getSVGsupport();
	browser.supportsMQ = getMQsupport();
	browser.supportsNthChild = getNthChildSupport();
	browser.supportsFlexbox = getFlexboxSupport();
	browser.viewportSize = getViewportSize();
	browser.isTouch = isTouch();
	browser.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	browser.viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

	me.onResize(getViewportSize);

	me.browser = browser;

	return me;
})();

(function() {
	$('html').removeClass('no-js');

	if (!APP.browser.supportsSVG) {
		$('html').addClass('no-svg');

		$('img').each(	function(n){
			var src = n.src;
			n.src = src.replace('.svg', '.png');
		});
	}

	if (!APP.browser.supportsMQ) {
		var respond = document.createElement('script');
		respond.src = '/js/respond.js';
		document.body.appendChild(respond);
	}

	if (!APP.browser.supportsNthChild) {
		//Test for nth-child support and add .clearrow class when not supported
	}


	var qSec = document.querySelector.bind(document),
		qSecA = document.querySelectorAll.bind(document),
		sliderContainer = qSec('#slider-container'),
		imgList = qSecA('#slider-container li'),
		imgAmount = imgList.length,
		navRadio = qSecA('input'),
		navAmount = navRadio.length,
		navBtn = qSecA('button'),
		navNext = qSec('#next'),
		navPrev = qSec('#prev'),
		imgCounter = 0;

	console.log(imgAmount);
	console.log(sliderContainer);
	console.log(navRadio);
	console.log(imgList);

	var showCurrent = function() {

		// Math.abs() to make to always positive.
		// It gives the remained of the imgAmount which is
		// the amount of items in the list. So 1 / 8 gives a
		// remainder of 1, and so on until 8 which gives a
		// remained of 0 and it repeats.
		var imgActive = Math.abs(imgCounter % imgAmount);
		console.log(imgActive);

		[].forEach.call( imgList, function(el){
			el.classList.remove('active');
		});

		[].forEach.call( navRadio, function(el){
			el.checked = false;
		});

		// element number [array value] add class or become checked.
		navRadio[imgActive].checked = true;
		imgList[imgActive].classList.add('active');

		// If navRadio of array value is checked,
		// than add active to the imgList with the same array
		// value.
	};

	navNext.addEventListener('click', function() {
		imgCounter++;
		showCurrent();
	}, false);

	navPrev.addEventListener('click', function() {
		// Stops it from cycling back in the wrong direction
		if (!imgCounter) { imgCounter = imgAmount; }
		imgCounter--;
		showCurrent();
	}, false);

	// Repeating this seems wasteful.
	sliderContainer.addEventListener('click', function() {
		navNext.click();
	});

	// Check that a key has been pressed.
	document.onkeydown = checkKey;
	// Makes the arrows keys scroll through next and prev slides.
	function checkKey(e) {
		// If the left arrow key is pressed move to prev slide,
		// else if the right is pressed move to next slide.
		if (e.keyCode == '37') {
			navPrev.click();
			showCurrent();
		} else if (e.keyCode == '39') {
			navNext.click();
			showCurrent();
		}
	}

})();